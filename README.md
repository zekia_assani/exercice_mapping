# Les enjeux/objectifs du projet

L'objectif principal de l'application est de visualiser sur un fond de carte numérique 
les arrêts de bus de la ville de Nantes et leurs métadata.

L'objectif sera atteint en deux étapes:
## Première étape:

```
Afficher sur un fond de carte les arrêts de bus de la ville de Nantes
Afficher sur le même fond de carte les arrêts de TRAM de la ville de Nantes
Proposer l'affichage (click droit ou passage sur le point) desmétadata sur les arrêts: (accès handicapé, correspondances Lignes, liens avec le Tram)
Relier graphiquement les arrêts avec un code couleur différentspour chaque ligne
```

## Deuxieme étape:

```
Relier graphiquement les arrêts avec un code couleur différents pour chaque ligne en fonction des routes disponibles sur l'Open Data de la ville de Nantes
Mettre une photo de l'arrêt : à faire pour quelques arrêts seulement
Proposer un filtrage sur l'affichage des metadata et sur ligne bus ou Tram
Proposer une légende explicite spour l'affichage
```

# Le rôle des devs dans le projet
        
    ASSANI Zékia: Développeur
    DIALLO Abdoulaye: Developpeur Senior

# Architecture et technologies du projet

![Semantic description of image](../../Images/architecture.png)

## Nom du projet: "Exercice Mapping"
## Prérequis: Python3.8 et PyCharm 2022.1
 
-[Installation de pyCharm](https://www.jetbrains.com/help/pycharm/installation-guide.html#standalone)
-[Installation de python](https://www.geeksforgeeks.org/download-and-install-python-3-latest-version/)

## Outils de gestion du projet: Trello 

-[Create your account](https://trello.com)
### Espace de travail du projet: "mapping_exercise"
### Tableau de travail: "Mapping"

    Toutes les taches à effectuer dans le projet sont listées dans la carte "A faire"
    Chaque tache est attribuée au développeur compétent par le chef de projet.
    Lorsqu'un collaborateur démarre uen tâche, il la déplace de "A faire" à "En cours"
    Une fois la tâche terminée par le développeur, il la place dans la carte "Review" pour la laisser valider par les autres
    La tâche une fois revue et validée, elle est mise dans la carte "Terminé" par le collaborateur en charge

## Outils de développement collaboratif: Gitlab

-[Create your account](https://gitlab.com/)
    
    Une fois ajouter au projet, vous devez le cloner (https://gitlab.com/zekia_assani/exercice_mapping.git)
## Politique de nomage:

### Les fichiers:
            On a un fichier par étape,
            Chaque fichier commence par une lettre majuscule avec le nom de l'etape
### Les branches:
            Pour chaque fonctionnalité, on crée une branche à partir de la branche main(branche principale)
            Chaque branche commence par le dénominateur "feature/" suivi du nom de la branche
            Pour chaque branche poussée, il faut qu'une merge soit créée
            La merge doit être approuvée par au moins un autre développeur
            Une fois approuvée, la branche peut être mergée dans la branche main par le développeur responsable de la branche

## Version à ce jour 12/05/2022: Vesion 1.0

        
# Points particuliers et risques du projet

        -Ambiguité dans les données récupérées
        -Inversion des coordonnées entre latitude et longitude 
        -Format de données incomplètes 

# Définition of Ready 

## Rédaction
    -Choix des technologies à utiliser
    -Redaction de l'architecture du projet
    -Definition et rédaction des différentes tâches dans Trello

## Développement
    -Creation du projet sur Gitlab
    -Rajout des collaborateurs 
    -Intégration de SonarQube dans le projet
    -Affichage des arrêts de bus de Nantes sur un fond de carte
    -Affichage des arrêts de tram sur le même fond de carte
    -Proposition de l'affichage des méta data sur les arrêts
    -Assemblage graphique des arrêts avec un code couleur 
    -Affichage graphique des arrêts avec un code couleur différents pour chaque ligne en fonction des routes disponibles
    -Mise en place d'une photo pour chaque arrêt
    -Proposition d'un filtrage sur l'affichage des metadata et sur ligne bus ou Tram
    -Proposition d'une légende explicite sur l'affichage

## Test
    Test des différentes fonctionnalités de développement


# Définition of Done
## Rédaction
    -Choix des technologies à utiliser
    -Redaction de l'architecture du projet
    -Definition et rédaction des différentes tâches dans Trello

## Développement
    -Creation du projet sur Gitlab
    -Rajout des collaborateurs 
    -Intégration de SonarQube dans le projet
    -Affichage des arrêts de bus de Nantes sur un fond de carte
    -Affichage des arrêts de tram sur le même fond de carte

