import folium
import pandas as pd
import branca

m = folium.Map(location=[47.218371, -1.553621], zoom_start=15)

dataset = pd.read_csv('stops.txt', sep=",")
bus = dataset['location_type'] == 0
tram = dataset['location_type'] == 1

bus_stop = dataset[bus]
tram_stop = dataset[tram]



arrets_bus = bus_stop['stop_name']
arrets_tram = tram_stop['stop_name']

dataset [ "wheelchair_boarding" ]=dataset [ "wheelchair_boarding" ].fillna(0)

def affichermeta(i) :
    nbhand = dataset [ "wheelchair_boarding" ].iloc [ i ]
    html = """<!DOCTYPE html>

          <html>
          <head> </head>

          <body>
            <ul>
               <li> Accès Handicapé : """ + str ( nbhand ) + """</li>
            </ul>
          </body>
          </html>


    """

    return html



for i in range(0, len(arrets_bus)):
    html = affichermeta ( i )
    iframe = branca.element.IFrame ( html = html , width = 510 , height = 280 )
    pop = folium.Popup ( folium.Html ( html , script = True ) , max_width = 500 )
    folium.Marker(
        [bus_stop['stop_lat'].iloc[i], bus_stop['stop_lon'].iloc[i]], tooltip=bus_stop['stop_name'].iloc[i],
        icon=folium.Icon(color='blue',popup=pop)
    ).add_to(m)


for j in range(0, len(arrets_tram)):
    html = affichermeta ( j )
    iframe = branca.element.IFrame ( html = html , width = 510 , height = 280 )
    pop = folium.Popup ( folium.Html ( html , script = True ) , max_width = 500 )
    folium.Marker(
        [tram_stop['stop_lat'].iloc[j], tram_stop['stop_lon'].iloc[j]], tooltip=tram_stop['stop_name'].iloc[j],
        icon=folium.Icon(color='green',popup=pop)
    ).add_to(m)


m.save('index.html')

